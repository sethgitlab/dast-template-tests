# DAST - Template Tests

This is a repo to test older versions of GitLab vendored DAST.gitlab-ci.yml files

The template files are copied into this repo from the [gitlab repo](https://gitlab.com/gitlab-org/gitlab).
The files are then modified slightly and put into the [ci-templates](./ci-templates) folder in order to use a WebGoat docker service.

This repo has branches for each distinct template and a gitlab-ci.yml file that pulls in that template. An MR has been opened for each branch, 
so that the status of the pipeline is readily available. 

Below is a history of the templates that were distributed with GitLab, along with which templates are the same. Templates in the same column are identical.

| Added DAST to Master | Updated for 12-0 | Added Full Scan Capabilities | Kubernetes Support Added | Template introduced |
|----------------------|------------------|------------------------------|--------------------------|---------------------|
| 12-4                 | 12-3             | 11-11                        | 11-10                    | 11-9                |
|                      | 12-2             |                              |                          |                     |
|                      | 12-1             |                              |                          |                     |
|                      | 12-0             |                              |                          |                     |



Prior to 11.9, templates were not included as part of GitLab. Sample templates were included in documentation. 

11.8 Documentation - https://gitlab.com/gitlab-org/gitlab/blob/11-8-stable-ee/doc/ci/examples/dast.md

11.7 Documentation - https://gitlab.com/gitlab-org/gitlab/blob/11-7-stable-ee/doc/ci/examples/dast.md

11.6 Documentation - https://gitlab.com/gitlab-org/gitlab/blob/11-6-stable-ee/doc/ci/examples/dast.md

11.5 Documentation - https://gitlab.com/gitlab-org/gitlab/blob/11-5-stable-ee/doc/ci/examples/dast.md

11.4 Documentation - https://gitlab.com/gitlab-org/gitlab/blob/11-4-stable-ee/doc/ci/examples/dast.md

In 11.9 documentation and prior DAST required the image `registry.gitlab.com/gitlab-org/security-products/zaproxy`.